import socket
import machine
import time

led = machine.Pin(16, machine.Pin.OUT)
pin = machine.Pin(4, machine.Pin.OUT)
adc = machine.ADC(0)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 8081))
s.listen(5)
while True:
    conn, addr = s.accept()
    print("Got a connection from %s" % str(addr))
    request = conn.recv(1024)
    print("Content = %s" % str(request))
    rec = request.decode('utf8')
    defvalue = rec.split(" ", 2)[1][1:]
    print(defvalue)
    html = ("Hello! The sensor value you specified is %s" % defvalue)
    while True:
        sensorvalue=adc.read()
        if sensorvalue > int(defvalue):
            led.off()
            pin.off()
            time.sleep_ms(100)
            pin.on()
        else:
            led.on()
        response = html
        conn.send(response)
        conn.close()
