import machine
import time

pin = machine.Pin(16, machine.Pin.OUT)
while True:
    pin.off()
    time.sleep_ms(500)
    pin.on()
    time.sleep_ms(500)
