import machine
import time

led = machine.Pin(16, machine.Pin.OUT)
button = machine.Pin(14, machine.Pin.IN, machine.Pin.PULL_UP)

while True:
    if not button.value():
        led.on()
    else:
        led.off()
    time.sleep_ms(100)
