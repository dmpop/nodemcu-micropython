import socket 
import machine

#Setup Socket WebServer
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 8081))
s.listen(5)
while True:
    conn, addr = s.accept()
    print("Got a connection from %s" % str(addr))
    request = conn.recv(1024)
    print("Content = %s" % str(request))
    rec = request.decode('utf8')
    sensorvalue = rec.split(" ", 2)[1][1:]
    print(sensorvalue)
    html = ("O hai, it's NodeMCU ESP8266. The sensor value you specified is %s" % sensorvalue)
    response = html
    conn.send(response)
    conn.close()
